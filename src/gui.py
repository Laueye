#coding: utf-8
#file name : gui.py
#Laueye - A program to analyze Laue diffraction diagrams.
#Copyright (C) <2008>  <Grissiom>

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

from PyQt4.QtGui import *
from PyQt4.QtCore import *
import sys

class image_dispaly(QWidget):
	'''Display the image, and provide some graphical function'''
	def __init__(self, parent = None):
		QWidget.__init__(self, parent)

class param_set(QWidget):
	'''User can set parameter to analyze the image here.'''
	def __init__(self, parent = None):
		QWidget.__init__(self, parent)

class result_dispaly(QWidget):
	'''Display the result.'''
	def __init__(self, parent = None):
		QWidget.__init__(self, parent)

class main_window(QMainWindow):
	def __init__(self, main_widget):
		QMainWindow.__init__(self)

		self.setCentralWidget(main_widget)
		self.creat_actions()
		self.creat_menu()
		self.creat_tool_bar()
		self.creat_st_bar()
		self.setWindowTitle(u"Laueye")

	def creat_actions(self):
		self.new_act = QAction(QIcon(":/images/new.png"), u"新图像(&N)", self)
		self.new_act.setShortcut("Ctrl+N")
		self.new_act.setStatusTip(u"新开始一个分析")
		self.connect(self.new_act, SIGNAL("triggered()"), self.new_)

		self.exit_act = QAction(u"退出(&x)", self)
		self.exit_act.setShortcut("Ctrl+Q")
		self.exit_act.setStatusTip(u"退出程序")
		self.connect(self.exit_act, SIGNAL("triggered()"), self, SLOT("close()"))

		self.about_act = QAction(u"关于(&A)", self)
		self.about_act.setStatusTip(u"关于这个软件")
		self.connect(self.about_act, SIGNAL("triggered()"), self.about_)

		self.help_act = QAction(u"帮助(&H)", self)
		self.help_act.setStatusTip(u"显示帮助")
		self.connect(self.help_act, SIGNAL("triggered()"), self.help_)

		self.about_qt_act = QAction(u"关于 QT(&Q)", self)
		self.about_qt_act.setStatusTip(u"关于 Qt")
		self.connect(self.about_qt_act, SIGNAL("triggered()"), qApp, SLOT("aboutQt()"))

	def creat_menu(self):
		self.file_menu = self.menuBar().addMenu(u"文件(&F)")
		self.file_menu.addAction(self.new_act)
		self.file_menu.addSeparator()
		self.file_menu.addAction(self.exit_act)

		self.help_menu = self.menuBar().addMenu(u"帮助(&H)")
		self.help_menu.addAction(self.help_act)
		self.help_menu.addSeparator()
		self.help_menu.addAction(self.about_act)
		self.help_menu.addAction(self.about_qt_act)

	def creat_tool_bar(self):
		pass
	
	def creat_st_bar(self):
		self.statusBar().showMessage(u"程序已启动")

	def new_(self):
		self.file_name = self.open_file()
		# DEBUG
#		print self.file_name

	def open_file(self):
		of_dialog = QFileDialog(self)
		of_dialog.setFileMode(QFileDialog.ExistingFile)
		of_dialog.setNameFilter(u"图像 (*.png *.jpg)")
		of_dialog.setFileMode(QFileDialog.ExistingFile)
		of_dialog.setWindowTitle(u"选择衍射图")

		if of_dialog.exec_() :
			file_n = of_dialog.selectedFiles()
			if not file_n.isEmpty():
				return str(file_n[0])

	def exit_(self):
		pass

	def about_(self):
		QMessageBox.about(self, u"关于 Laueye", 
				u"一个解析劳厄衍射图的程序。")

	def help_(self):
		pass

	def help_qt(self):
		pass

app = QApplication(sys.argv)

im_ori = QImage()
im_ori.load('../test/test.png')
pain = SimpleExampleWidget(im_ori)

window =main_window(pain)
window.setGeometry(0, 0, im_ori.width(), im_ori.height())

window.show()

sys.exit(app.exec_())

