#coding: utf-8
#file name: graphical_analysis_pyqt4.py
#Laueye - A program to analyze Laue diffraction diagrams.
#Copyright (C) <2008>  <Grissiom>
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

import Image
import ImageFilter
import numpy

class circle:
	'''a circle's data'''
	def __init__(self):
		self.edge = []
		self.x_boundry = []
		self.y_boundry = []
	def center(self):
		'''return the location of the circle, in flaot format'''
		x = 0
		y = 0
		for i in self.edge:
			x += i[0]
			y += i[1]
		return float(x) / len(self.edge), float(y) / len(self.edge)

scope = 2
'''设置程序扫描的范围'''

cl_size_min = 5
'''define the min size of a circle, used to swapt put noise.'''

cl_size_max = 500
'''define the max size of a circle, used to swapt put noise.'''

threshold = 7
'''define the threshold of the spot'''

def figure_out_circles(im_in):
	'''figure out the circles in it

	@param im_in the img to analyz
	@return return a list of circles
	'''
	im_in = denoise_img(im_in)
	gray_table = conv_to_array(im_in)
	width, height = im_in.size
	assert gray_table.shape == (height, width)
	status = []
	for i in range(height):
		status.append([])
		for k in range(width):
			status[i].append(0)
	find_edge(gray_table, status, im_in)
	return _get_circles(status)

def denoise_img(im_in):
	'''return a denoised img

	@param im_in img in
	@return im_out img out'''
	im_out = im_in.filter(ImageFilter.BLUR)
	im_out = im_out.filter(ImageFilter.BLUR)
	return im_out

def conv_to_array(im_in):
	'''convert the given img to numpy.narray

	convert the given img to numpy.narray, each item indicates the gray scale.
	@param im_in the img to convert
	@return nar narray that convert from img'''
	width, height = im_in.size
	nar = numpy.zeros((height, width), 'l')
	im_in = im_in.convert('L')
	pix = im_in.load()
	for i in range(width):
		for k in range(height):
			nar[k, i] = pix[i, k]
	return nar

def find_edge(nar_in, status, im_in):
	'''determine edge in nar_in.

	set edge status to 2.
	@param nar_in the gray scaled matrix
	@param status the status table to set up'''
	height, width = nar_in.shape
	size = 2
	k = size
	while k < width - size:
		i = size
		while i < height - size:
			gr1 = numpy.average(nar_in[i - size:i, k - size:k])
			gr2 = numpy.average(nar_in[i:i + size, k - size:k])
			gr3 = numpy.average(nar_in[i:i + size, k:k + size])
			gr4 = numpy.average(nar_in[i - size:i, k:k + size])
			if max(gr1, gr2, gr3, gr4) - min(gr1,  gr2,  gr3,  gr4) > threshold:
				status[i][k] = 2
				im_in.putpixel((k, i), (255, 0, 0))
			i += size
		k += size
	im_in.save('../test/test-pred.png')


def _get_boundry(in_list, status):
	'''get the boundry of the whole circle based on the status table.

	@param in_list a point on the circle.
	@param status the status table which is the base to analyze
	@return edge a list of tuples which are the edge of the circle, in (x, y) format.
	'''
	edge = []
	while len(in_list) != 0:
		y, x = in_list.pop()
		edge.append((y, x))
		status[y][x] = 4
		for i in range(-scope, scope + 1):
			for k in range(-scope, scope + 1):
				if status[y + i][x + k] == 2:
					in_list.append([y + i, x + k])
	return edge

def _get_circles(status):
	'''get the circles in the whole image.

	@param status the status table which is the base of analyze.
	@return a list of circles.'''
	width = len(status)
	height = len(status[0])
	#TODO:maybe we need to check whether im_status is a matrices here.
	# i.e, all the len(im_status[:]) have the same value.
	circles = []
	for i in range(scope,  width - scope):
		for k in range(scope,  height - scope):
			if status[k][i] == 2:
				circles.append(circle())
				circles[-1].edge = _get_boundry([[k, i]], status)
				if len(circles[-1].edge) < cl_size_min or len(circles[-1].edge) > cl_size_max:
					del circles[-1]
	return circles

if __name__ == '__main__':
	circles = figure_out_circles(Image.open('../test/test_laue.jpg'))
#	circles = figure_out_circles(Image.open('../test/test.png'))
	print 'we have', len(circles),'circles'
	#print 'thay are', [i.center() for i in circles]
else:
	pass
pass

